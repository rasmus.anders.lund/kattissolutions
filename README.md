Problems
======



| Problem ID                                                                                                  | Solution                       |
|-------------------------------------------------------------------------------------------------------------|--------------------------------|
| [This ain't your grandpas checker board](https://open.kattis.com/problems/thisaintyourgrandpascheckerboard) | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/thisAintYourGranpasCheckerboard) |
| [Parking](https://open.kattis.com/problems/parking)                                                         | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/Parking)                         |
| [Secret Message](https://open.kattis.com/problems/secretmessage)                                            | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/SecretMessage)                   |
| [Calories From Fat](https://open.kattis.com/problems/calories)                                              | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/caloriesFromFat)                 | 
| [Not Amused](https://open.kattis.com/problems/notamused)                                                    | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/notAmused)                       |
| [Lawn Mower](https://open.kattis.com/problems/lawnmower)                                                    | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/lawnMower)                       |
| [Time Travelling Temperatures](https://open.kattis.com/problems/temperature)                                | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/TimeTravellingTemperatures)      |
| [Slatkisi](https://open.kattis.com/problems/slatkisi)                                                       | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/Slatkisi)                        |
| [Backspace](https://open.kattis.com/problems/backspace)                                                     | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/Backspace)                       |
| [Where's My Internet??](https://open.kattis.com/problems/wheresmyinternet)                                  | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/whereIsMyInternet)               |
| [Guess the number](https://open.kattis.com/problems/guess)                                                  | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/guessTheNumber)                  |
| [Climbing Worm](https://open.kattis.com/problems/climbingworm)                                              | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/ClimbingWorm)                    |
| [Treasure Hunt](https://open.kattis.com/problems/treasurehunt)                                              | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/treasureHunt)                    |
| [Waif Until Dark](https://open.kattis.com/problems/waif)                                                    | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/wifUntilDark)                    |
| [Snapper Chain (Easy)](https://open.kattis.com/problems/snappereasy)                                        | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/snapperChain)                    |
| [Awkward Party](https://open.kattis.com/problems/awkwardparty)                                              | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/akwardParty)                     |
| [Amalgamated Artichokes](https://open.kattis.com/problems/artichoke)                                        | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/amalgamatedAtrichokes)           |
| [Toilet Seat](https://open.kattis.com/problems/toilet)                                                      | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/toiletSeat)                      |
| [Touchscreen Keyboard](https://open.kattis.com/problems/touchscreenkeyboard)                                | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/touchScreenKeyboard)             |
| [What does the fox say?](https://open.kattis.com/problems/whatdoesthefoxsay)                                | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/whatdoesthefoxsay)               |
| [Warehouse](https://open.kattis.com/problems/warehouse)                                                     | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/warehouse)                       |
| [Sort](https://open.kattis.com/problems/sort)                                                               | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/sort)                            |  
| [Turtle Master](https://open.kattis.com/problems/turtlemaster)                                              | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/tree/master/KattisSolutions/src/turtleMaster)                    | 
| [Joyless Game](https://open.kattis.com/problems/joylessgame)                                                | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/-/tree/master/KattisSolutions/src/joylessGame)                   | 
| [Bumper-To-Bumper Traffic](https://open.kattis.com/problems/traffic)                                        | [Java](https://gitlab.com/rasmus.anders.lund/kattissolutions/-/tree/master/KattisSolutions/src/traffic)                       | 
