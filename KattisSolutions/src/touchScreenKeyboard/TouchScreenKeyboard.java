package touchScreenKeyboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class TouchScreenKeyboard {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer ts = new StringTokenizer(reader.readLine());

		int cases = Integer.parseInt(ts.nextToken());

		int[][] ss = { { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 6, 0 }, { 7, 0 }, { 8, 0 },
				{ 9, 0 }, { 0 , 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 7, 1 }, { 8, 1 },
				{ 0, 2 }, { 1, 2 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 6, 2 } };
		Character[] c = { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
				'z', 'x', 'c', 'v', 'b', 'n', 'm' };

		HashMap<Character, int[]> coords = new HashMap<Character, int[]>();
		for (int i = 0; i < ss.length; i++) {
			coords.put(c[i], ss[i]);
		}
	
		
		LinkedList<String> words = new LinkedList<String>();
		LinkedList<Integer> distances = new LinkedList<Integer>();

		for (int i = 0; i < cases; i++) {
			ts = new StringTokenizer(reader.readLine());
			String word = ts.nextToken();
			int tests = Integer.parseInt(ts.nextToken());
			for (int j = 0; j < tests; j++) {
				String w = reader.readLine();
				int dist = 0;
				for (int t = 0; t < word.length(); t++) {
					dist += (Math.abs(coords.get(word.charAt(t))[0] - coords.get(w.charAt(t))[0])); // difference in x
																									// coordinates
					dist += (Math.abs(coords.get(word.charAt(t))[1] - coords.get(w.charAt(t))[1])); // difference in y
																									// coordinates
				}

				if (distances.size() == 0) {
					distances.add(dist);
					words.add(w);
				} else {
					for (int t = 0; t < distances.size(); t++) {

						if (dist < distances.get(t)) {
							distances.add(t, dist);
							words.add(t, w);
							break;
						}

						if (dist == distances.get(t)) {
								if (words.get(t).compareTo(w) > 0) {
									words.add(t, w);
									distances.add(t, dist);
									break;
								}
								if (t == distances.size() -1) {
									words.add(w);
									distances.add(dist);
									break;
								}
						}
						
						
						
						if (dist > distances.get(t) && t == distances.size() - 1) {
							distances.add(dist);
							words.add(w);
							break;
						}
					}
				}

			}
			for (int t = 0; t < distances.size(); t++) {
				System.out.println(words.get(t) + " " + distances.get(t));
			}

			distances.clear();
			words.clear();
		}
	}

}
