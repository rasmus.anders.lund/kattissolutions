package akwardParty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;


public class AkwardParty {

	public static void main(String[] args) throws IOException {
		 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	     StringTokenizer ts = new StringTokenizer(reader.readLine());

	     int n = Integer.parseInt(ts.nextToken());
	     ts = new StringTokenizer(reader.readLine());
	     
	     
	     HashMap<Long,Integer> map = new HashMap<Long,Integer>();
	     boolean sameLanguage = false;
	     long akward = n;
	     
	     for (int i = 0; i < n; i++) {
	    	 if (akward == 1)     //it can not get any less akward
	    		 break;
	    	 long k = Long.parseLong(ts.nextToken());
	    	 if (map.containsKey(k)) {
	    		 sameLanguage = true;
	    		 akward = Math.min(i-map.get(k), akward);
	    		 map.put(k, i);
	    		 
	    	 }
	    	 map.put(k, i);
	     }
	     if (sameLanguage)
	    	 System.out.println(akward);
	     else
	    	 System.out.println(n);
	     
	}

}
