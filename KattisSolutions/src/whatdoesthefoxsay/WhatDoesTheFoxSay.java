package whatdoesthefoxsay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class WhatDoesTheFoxSay {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int cases = Integer.parseInt(reader.readLine());
		
		for (int test = 0; test < cases; test++) {
		StringTokenizer ts = new StringTokenizer(reader.readLine());
		HashMap<String,Integer> sound = new HashMap<String,Integer>();
		
		String [] recording = new String[ts.countTokens()];

		
		for (int i = 0; i < recording.length; i++) {
			recording[i] = ts.nextToken();
		 }
	
		while (reader.ready()) {
			String s = reader.readLine();
			if (s.substring(0, 9).equals("what does"))
				break;
			sound.put(s.substring(s.lastIndexOf(" ")+1), 1);
		 }
		for (String st : recording) {
			if(!sound.containsKey(st))
				System.out.print(st + " ");
			}
		System.out.println();
		}

	}

}
