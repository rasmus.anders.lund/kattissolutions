package wifUntilDark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;
/**Imagine a flow-graph where source node are connected to all kids, with a max flow of 1 on each 
*  path, and kids are connected to the toys they want, with a max flow of one, and toys are connected 
*  to the group they belong to with a max flow of one, and the group node is connected to the sink node
*  with a max flow of the maximum allowed usage of toys from that group.	
*/

public class WaifUntilDark2 {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer ts = new StringTokenizer(reader.readLine());
		
		int n = Integer.parseInt(ts.nextToken());
		int m = Integer.parseInt(ts.nextToken());
		int p = Integer.parseInt(ts.nextToken());
		int A = n+m+p;
		int graph [][] = new int [A+2][A+2];
		HashMap <Integer,Integer> toys = new HashMap<Integer,Integer>();
		
		//source node to kids
		for (int i = 0; i <= n; i++) {
			if (i > 0)
				graph[0][i] = 1;
		}
		
		// kids to toys
		for (int i = 1; i <= n; i++) {
			ts = new StringTokenizer(reader.readLine());
			int x = Integer.parseInt(ts.nextToken());
			for (int j = 0; j < x; j++) {
				int a = Integer.parseInt(ts.nextToken());
				graph[i][n+a] = 1;	
			}
		}
		
		// Toys to groups
		for (int i = 1; i <= p; i++) {
			ts = new StringTokenizer(reader.readLine());
			int x = Integer.parseInt(ts.nextToken());
			int t [] = new int[x];
			for (int j = 0; j < x; j++) {
				t[j] = Integer.parseInt(ts.nextToken());
			}
			x = Integer.parseInt(ts.nextToken());
			for (int j : t)
				toys.put(j, i);
			graph[i+n+m][A+1] = x;
		}
		
		// Groups to sink node, or toys directly to sink node
		for (int i = 1; i <= p; i++) {
			for (int j = 1; j <= m; j++) {
				if (toys.containsKey(j)) 
					graph[n+j][m+n+toys.get(j)] = 1;
				else 
					graph[j+n][A+1] = 1;
			}
		}
		if (p == 0) {
			for (int j = 1; j <= m; j++) {
				graph[j+n][A+1] = 1;
			}	
		}
		
		System.out.println(fordFulkerson(graph,0,A+1,A+2,n,m));
	    
		
	}
	
	/**
	 *  Returns true if there is a path from source {@code s} to sink {@code t} and stores path in {@code t}
	 * @param rGraph the graph
	 * @param s source node
	 * @param t sink node
	 * @param parent path
	 * @param V number of nodes
	 * @return true of we reach the sink node {@code t}
	 */
  static boolean bfs(int rGraph[][], int s, int t, int parent[], int V,int n, int m) { 
      // Create a visited array and mark all vertices as not 
      // visited 
      boolean visited[] = new boolean[V]; 
      for(int i=0; i<V; ++i) 
          visited[i]=false; 

      // Create a queue, enqueue source vertex and mark 
      // source vertex as visited 
      LinkedList<Integer> queue = new LinkedList<Integer>(); 
      queue.add(s); 
      visited[s] = true; 
      parent[s]=-1; 

      // Standard BFS Loop 
      while (queue.size()!=0) { 
          int u = queue.poll(); 

          if (u != 0 && u < n ) {
        	  for (int v = u; v < n+m; v++) {
        		  if (visited[v]==false && rGraph[u][v] > 0){ 
                      queue.add(v); 
                      parent[v] = u; 
                      visited[v] = true;
        	  }
          }
          }
          else {
        	  
          for (int v=1; v<V; v++) {        	  
              if (visited[v]==false && rGraph[u][v] > 0){ 
                  queue.add(v); 
                  parent[v] = u; 
                  visited[v] = true; 
              } 
          }
          }
      } 

      return (visited[t] == true); 
  }
  
  /**
   * 
   * @param graph
   * @param s source node
   * @param t sink node
   * @param V number of nodes
   * @return max flow (number of kids who get a toy)
   */
  static int fordFulkerson(int graph[][], int s, int t, int V,int n, int m){ 
      int u, v; 

      // Create a residual graph and fill the residual graph 
      // with given capacities in the original graph as 
      // residual capacities in residual graph 

      // Residual graph where rGraph[i][j] indicates 
      // residual capacity of edge from i to j (if there 
      // is an edge. If rGraph[i][j] is 0, then there is 
      // not) 
      int rGraph[][] = new int[V][V]; 

      for (u = 0; u < V; u++) 
          for (v = 0; v < V; v++) 
              rGraph[u][v] = graph[u][v]; 

      // This array is filled by BFS and to store path 
      int parent[] = new int[V]; 

      int max_flow = 0;   

      // Augment the flow while there is path from source 
      // to sink 
      while (bfs(rGraph, s, t, parent, V, n, m)){ 
          // Find minimum residual capacity of the edges 
          // along the path filled by BFS. Or we can say 
          // find the maximum flow through the path found. 
          int path_flow = 0; 
          for (v=t; v!=s; v=parent[v]){ 
              u = parent[v]; 
              path_flow = rGraph[u][v]; 
          } 

          // update residual capacities of the edges and 
          // reverse edges along the path 
          for (v=t; v != s; v=parent[v]) { 
              u = parent[v]; 
              rGraph[u][v] -= path_flow; 
              rGraph[v][u] += path_flow; 
          } 

          // Add path flow to overall flow 
          max_flow += path_flow; 
      } 

      // Return the overall flow 
      return max_flow; 
  }
}
