package snapperChain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class SnapperChain {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer ts = new StringTokenizer(reader.readLine());
        int t = Integer.parseInt(ts.nextToken());
        
        ts = new StringTokenizer(reader.readLine());
        int [] nu = new int[10];
        for (int i = 0; i < 10; i++) {
            nu[i] = i+1;
        }
        for (int i = 1; i < 10; i++) {
            for (int j = 0; j < i; j++ ) {
                nu[i] = nu[i] + nu[j];
            }
        }
        
        for (int i = 0; i < t; i++ ) {
            
            int n = Integer.parseInt(ts.nextToken());
            int k = Integer.parseInt(ts.nextToken());
            if (k < nu[n-1]){
                System.out.printf("Case #%d: OFF %n", (i+1));
                if (i != t-1) {
                ts = new StringTokenizer(reader.readLine());
                continue;}
                break;
            }
            
            
            int r = nu[n-1];
        
            while (r < k) {
                r = nu[n-1]+r+1;
            }
            if (r != k)
                System.out.printf("Case #%d: OFF %n", (i+1));
            else 
                System.out.printf("Case #%d: ON %n", (i+1));
            
            if(i != t-1)
            ts = new StringTokenizer(reader.readLine());    
        }

    }

}