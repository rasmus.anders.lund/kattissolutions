package whereIsMyInternet;

import java.util.LinkedList;


public class Graph {
    private final int V;
    private int E;
    private LinkedList<Integer>[] adj;



    @SuppressWarnings("unchecked")
	public Graph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        adj = new LinkedList[V];
        for (int v = 0; v < V; v++) {
            adj[v] = new LinkedList<Integer>();
        }
    }

public int V() {
    return V;
}
public int E() {
    return E;
}
public void addEdge(int v, int w) {
    E++;
    adj[v].add(w);
    adj[w].add(v);
}
public Iterable<Integer> adj (int v){
	return adj[v];
}
}