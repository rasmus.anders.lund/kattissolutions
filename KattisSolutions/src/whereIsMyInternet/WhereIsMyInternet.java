package whereIsMyInternet;



public class WhereIsMyInternet {

	public static void main(String[] args) {
		
		Kattio inn = new Kattio(System.in);
		int houses = inn.getInt();
		StringBuilder output = new StringBuilder();
		int cables = inn.getInt();
		
		Graph g = new Graph(houses);
		
		for(int i = 0; i<cables;i++) {
			g.addEdge(inn.getInt()-1, inn.getInt()-1);
		}
	
		
	
	   DepthFirstSearch deep = new DepthFirstSearch(g,0);
	   
	  
	   
	   for(int i = 0; i < g.V(); i++) {
		   if (!deep.marked(i))
			   output.append((i+1) + " ");
	   }
	   
	   if (output.length()>=1)
		   System.out.println(output);
	   else
		   System.out.println("Connected");
	   
	   inn.close();
	}
	
}
