package mia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Mia {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		 
		int player1 = 0;
		int player2 = 0;
		
		while (true) {
			StringTokenizer st = new StringTokenizer(reader.readLine());
			
			char [] p1 = {st.nextToken().toCharArray()[0], st.nextToken().toCharArray()[0]};
			char [] p11 = {p1[1],p1[0]};
			if (p1[0] == '0')
				break;
			
			char [] p2 = {st.nextToken().toCharArray()[0], st.nextToken().toCharArray()[0]};
			char [] p22 = {p2[1],p2[0]};
			
			player1 = Math.max(Integer.parseInt(new String(p1)), Integer.parseInt(new String(p11)));
			player2 = Math.max(Integer.parseInt(new String(p2)), Integer.parseInt(new String(p22)));
			
			player1 = eval(player1);
			player2 = eval(player2);
			
			if (player1 > player2)
				System.out.println("Player 1 wins.");
			else if(player2 > player1)
				System.out.println("Player 2 wins.");
			else 
				System.out.println("Tie.");
		}

	}
	private static int eval(int score) {
		if (score == 11)
			return 67;
		if (score == 22)
			return 68;
		if (score == 33)
			return 69;
		if (score == 44)
			return 70;
		if (score == 55)
			return 71;
		if (score == 66)
			return 72;
		if (score == 21)
			return 73;
		return score;
	}

}
