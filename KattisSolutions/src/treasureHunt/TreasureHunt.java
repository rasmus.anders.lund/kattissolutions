package treasureHunt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class TreasureHunt {
	public static void main (String [] args) throws IOException {
		BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(reader.readLine());
		
		int R = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		int steps = 0;
		int x = 0;
		int y = 0;
		final int MAXSTEPS = C*R;
		String [] rows = new String[R];
		rows[y] = reader.readLine();
		
		while (true) {
		String	s = rows[y];
			char c = s.charAt(x);
			
			if (steps >= MAXSTEPS) {
				System.out.print("lost");
				break;
			}
			
			switch (c) {
			case 'N':
				y--;
				steps++;
				if (x >= C || x < 0 || y >= R || y < 0) {
					System.out.print("Out");
					System.exit(0);
				}
				break;
				
			case 'S':
				y++;
				steps++;
				if (x >= C || x < 0 || y >= R || y < 0) {
					System.out.print("Out");
					System.exit(0);
				}
				if (rows[y] == null)
				rows[y] = reader.readLine();
				break;
				
			case 'E':
				x++;
				steps++;
				if (x >= C || x < 0 || y >= R || y < 0) {
					System.out.print("Out");
					System.exit(0);
				}
				break;
				
			case 'W':
				x--;
				steps++;
				if (x >= C || x < 0 || y >= R || y < 0) {
					System.out.print("Out");
					System.exit(0);
				}
				
				break;
			case 'T':
				System.out.println(steps);
				System.exit(0);
			
			}
			
		}
	}
}