package Slatkisi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Slatkisi {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		 StringTokenizer st = new StringTokenizer(reader.readLine());
		 int price = Integer.parseInt(st.nextToken());
		 double smallestBill = Math.pow(10, Double.parseDouble(st.nextToken()));
		 System.out.printf("%.0f\n", Math.round(price/smallestBill)*smallestBill);
	}	

}
