package thisAintYourGranpasCheckerboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GranpasCheckerBoard {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int size = Integer.parseInt(reader.readLine());

		char[][] brett = new char[size][size];
		int b = 0;
		int[] black = new int[size];
		for (int i = 0; i < size; i++) {
			b = 0;
			String s = reader.readLine();
			for (int j = 0; j < size; j++) {
				brett[i][j] = s.charAt(j);
				if (brett[i][j] == 'B') {
					b++;
					black[j] = black[j] + 1;
				}
			}
			if (b != size / 2) {
				System.out.println(0);
				System.exit(0);
			}

		}
		for (int i = 0; i < size; i++) {
			if (black[i] != size / 2) {
				System.out.println(0);
				System.exit(0);
			}
		}
		for (int i = 0; i < size - 2; i++) {
			for (int j = 0; j < size - 2; j++) {
				if (brett[i][j] == brett[i + 1][j] && brett[i][j] == brett[i + 2][j]
						|| brett[i][j] == brett[i][j + 1] && brett[i][j] == brett[i][j + 2]) {
					System.out.println(0);
					System.exit(0);
				}
			}
		}
		System.out.println(1);

	}
}
