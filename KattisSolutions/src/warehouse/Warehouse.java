package warehouse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Warehouse {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int cases = Integer.parseInt(reader.readLine());
		
		for (int i = 0; i < cases; i++) {
			int n = Integer.parseInt(reader.readLine());
			HashMap<String,Integer> toys = new HashMap<String,Integer>();
			LinkedList<LinkedList> sa = new LinkedList<LinkedList>();
			
			String [] to;
			for (int j = 0; j < n; j++ ) {
				StringTokenizer st = new StringTokenizer(reader.readLine());
				String k = st.nextToken();
				if (toys.containsKey(k)) {
					int t = toys.get(k);
					toys.replace(k, t+Integer.parseInt(st.nextToken()));
					continue;
				}
				toys.put(k, Integer.parseInt(st.nextToken()));
			}
			Map<String,Integer> sortedMap = toys.entrySet().stream().sorted(Entry.<String,Integer>comparingByValue().reversed()).collect(Collectors.toMap(Entry::getKey, Entry::getValue,(e1, e2) -> e1, LinkedHashMap::new));
			System.out.println(toys.size());
			
			int lastval = 0;
			int index = 0;
			for (Map.Entry<String,Integer> entry : sortedMap.entrySet()) {
				String key = entry.getKey();
				Integer val = entry.getValue();	
				if (val == lastval) {
					//sa.add(new LinkedList<String>());
					sa.get(index-1).add(key + " " + val.toString());
					Collections.sort(sa.get(index-1));
					continue;
				}
				sa.add(new LinkedList<String>());
				sa.get(index).add(key + " " + val.toString());
				index++;
				lastval = val;
			}
			for(int v = 0; v < sa.size(); v++) {
				for (int o = 0; o < sa.get(v).size(); o++) {
					System.out.println(sa.get(v).get(o));
				}
				
			}
		}

	}

}
