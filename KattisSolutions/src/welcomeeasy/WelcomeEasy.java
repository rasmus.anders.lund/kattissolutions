package welcomeeasy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.StringTokenizer;

public class WelcomeEasy {
	static char[] goal = "welcome to code jam".toCharArray();
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int cases = Integer.parseInt(reader.readLine());
		
		int c =0;
		for (int i = 0; i < cases; i++) {
			char[] line = reader.readLine().toCharArray();
			System.out.printf("Case %d: %04d \n",++c, permutations(0,0,line));
		}

			
			
		
	}
	private static int permutations(int i, int j, char[] line) {
		if (j == goal.length) {
            return 1;
        } else if (i == line.length) {
            return 0;
        }
		int tot = permutations(i+1,j,line);
		if (line[i] == goal[j]) {
			tot += permutations(i+1,j+1,line);
		}
		return tot;
	}
}
