package amalgamatedAtrichokes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class AmalgamatedArtichokes {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	    StringTokenizer ts = new StringTokenizer(reader.readLine());
	    double high = Double.MIN_VALUE;
	    double low = Double.MIN_VALUE;
	    
	    double p = Double.parseDouble(ts.nextToken());
	    double a = Double.parseDouble(ts.nextToken());
	    double b = Double.parseDouble(ts.nextToken());
	    double c = Double.parseDouble(ts.nextToken());
	    double d = Double.parseDouble(ts.nextToken());
	    int n = Integer.parseInt(ts.nextToken());
	    double decline = 0;
	    
	    for (int i = 1; i <= n; i++) {
	    	double price = p*(Math.sin(a*i+b)+(Math.cos(c*i+d)+2));
	    	if (i == 1) {
	    		high = price;
	    		continue;
	    	}
	    	if (price > high) {
	    		high = price;
	    		continue;
	    	}
	    	if (price < high) {
	    		low = price;
	    		double diff = high-low;
	    		if (diff > decline)
	    			decline = diff;
	    	}
	    }
	    System.out.println(decline);

	}

}
