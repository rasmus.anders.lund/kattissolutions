package lawnMower;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class LawnMower {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(reader.readLine());
        
        int w = Integer.parseInt(st.nextToken());
        int h = Integer.parseInt(st.nextToken());
        double width = Double.parseDouble(st.nextToken());
        double length = width/2;
        boolean possible = true;
        
        while (w != 0 && width != 0 && h != 0) {
            PriorityQueue <Double> qh = new PriorityQueue<Double>();
            PriorityQueue <Double> qw = new PriorityQueue<Double>();
            st = new StringTokenizer(reader.readLine());
            for (int i = 0; i < w; i++) {
                qw.add(Double.parseDouble(st.nextToken())-length);
            }
            
            for (int i = 0; i < w-1; i++) {
                if (i == 0 && possible)
                    possible = (qw.peek() <= 0);
                if ((qw.poll() + width) < qw.peek()) {
                    possible = false;
                    break;
                }
                if (i == w-2 && possible)
                    possible = ((qw.peek()+width) >= 75);
                    
            }
            
            st = new StringTokenizer(reader.readLine());
            if (possible) {
            for (int i = 0; i < h; i++) {
                qh.add(Double.parseDouble(st.nextToken())-length);
            }
            for (int i = 0; i < h-1; i++) {
                if (i == 0 && possible)
                    possible = (qh.peek() <= 0);
                if ((qh.poll()+width) < qh.peek()) {
                    possible = false;
                    break;
                }
                if (i == h-2 && possible)
                    possible = ((qh.peek()+width) >= 100);
            }
            }
            if (possible)
                System.out.println("YES");
            else
                System.out.println("NO");
            st = new StringTokenizer(reader.readLine());
            w = Integer.parseInt(st.nextToken());
            h = Integer.parseInt(st.nextToken());
            width = Double.parseDouble(st.nextToken());
            length = width/2;
            possible = true;
        }
    }

}