package guessTheNumber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Guess {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int guess = 500;
		int high = 1000;
		int low = 1;
		String inn = "";
		for (int i = 0; i < 10; i++) {
			System.out.println(guess);
			System.out.flush();
			inn = reader.readLine();
			if (inn.equals("lower")) {
				high = guess-1;
				guess = (low+high)/2;
				continue;
			}
			if (inn.equals("higher")) {
				low = guess+1;
				guess = (high+low)/2;
				continue;
			}
			if (inn.equals("correct"))
				System.exit(0);
		}

	}

}
