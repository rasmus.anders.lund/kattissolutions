package ClimbingWorm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class climbingWorm {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer ts = new StringTokenizer(reader.readLine());
		
		int up = Integer.parseInt(ts.nextToken());
		int fall = Integer.parseInt(ts.nextToken());
		int goal = Integer.parseInt(ts.nextToken());
		
	
		int ans = 0;
		int height = 0;
		while(true) {
			height = height+up;
			ans++;
			if (height >= goal)
				break;
			height -= fall;
		}
		
		System.out.println(ans);
	}

}
