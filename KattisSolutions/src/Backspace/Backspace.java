package Backspace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Backspace {
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int fjern = 0;
		Scanner tast = new Scanner(System.in);
		String input = reader.readLine();
		char[] in = input.toCharArray();
		
		for (int i = in.length-1; i>=0; i--){
			if (in[i]=='<'){
				in[i] = ' ';
				fjern++;
			}
			else if(fjern>0){
				in[i] = ' ';
				fjern--;
			}
		}
		input = new String(in);
		System.out.println(input.replace(" ", ""));
		tast.close();		
	}
}
