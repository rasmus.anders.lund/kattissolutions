package notAmused;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NotAmused {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int day = 0;
        TreeMap<String,Double> customers = new TreeMap<String,Double>();
        
        String s = "";
        String [] log = new String[3];
        while ((s = reader.readLine())!= null) {

            while (!(s=reader.readLine()).equals("CLOSE")) {
                StringTokenizer st = new StringTokenizer(s);
                log[0] = st.nextToken();
                log[1] = st.nextToken();
                log[2] = st.nextToken();
                if (customers.containsKey(log[1])) {
                    double val;
                    val = customers.get(log[1]);
                    if (log[0].equals("ENTER")) 
                        customers.replace(log[1], val+(Double.parseDouble(log[2])));
                    if (log[0].equals("EXIT"))
                        customers.replace(log[1], val-(Double.parseDouble(log[2])));
                    }else {
                        customers.put(log[1], Double.parseDouble(log[2]));
                    }
            }
            day++;
            System.out.println("Day " + day);
            for (Map.Entry<String, Double> entry : customers.entrySet()) {
                String k = entry.getKey();
                double d = entry.getValue();
                System.out.printf("%s $%.2f \n", k, Math.abs(d*0.1));
            }
            System.out.println();
            customers.clear();
        }
    }

}