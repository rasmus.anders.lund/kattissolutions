package turtleMaster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TurtleMaster {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		String [] rows = new String[8];
		for(int i = 7; i > -1; i--) {
			rows[i] = reader.readLine();
		}
		String commands = reader.readLine();
		int xpos = 0;
		int ypos = 0;
		boolean right = true;
		boolean left = false;
		boolean up =false;
		boolean down = false;
		
		for (int i = 0; i < commands.length(); i++) {
			switch(commands.charAt(i)) {
			case 'F':
				if (right) {
					xpos++;
					validState(xpos,ypos,rows);
					break;
				}
				if (left) {
					xpos--;
					validState(xpos,ypos,rows);
					break;
				}
				if (up) {
					ypos++;
					validState(xpos,ypos,rows);
					break;
				}
				if (down) {
					ypos--;
					validState(xpos,ypos,rows);
					break;
				}
				break;
			case 'L':
				if (right) {
					right = false;
					up = true;
					break;
				}
				if (left) {
					left = false;
					down = true;
					break;
				}
				if (up) {
					up = false;
					left = true;
					break;
				}
				if (down) {
					down = false;
					right = true;
					break;
				}
			case 'R':
				if (right) {
					right = false;
					down = true;
					break;
				}
				if (left) {
					left = false;
					up = true;
					break;
				}
				if (up) {
					up = false;
					right = true;
					break;
				}
				if (down) {
					down = false;
					left = true;
					break;
				}
			case 'X':
				if (right) {
					if (xpos == 7) {
						System.out.println("Bug!");
						System.exit(0);
					}
					if (rows[ypos].charAt(xpos+1) == 'I') {   // delete icetower
						char [] s = rows[ypos].toCharArray();
						s [xpos+1] = '.';
						String ss = new String(s);
						rows[ypos] = ss;
					} else {
						System.out.println("Bug!");
						System.exit(0);
					}
					break;
				}
				if (left) {
					if (xpos == 0) {
						System.out.println("Bug!");
						System.exit(0);
					}
					if (rows[ypos].charAt(xpos-1) == 'I') {   // delete icetower
						char [] s = rows[ypos].toCharArray();
						s [xpos-1] = '.';
						rows[ypos] = new String(s);
					} else {
						System.out.println("Bug!");
						System.exit(0);
					}
					break;
				}
				if (up) {
					if (ypos == 7) {
						System.out.println("Bug!");
						System.exit(0);
					}
					if (rows[ypos+1].charAt(xpos) == 'I') {   // delete icetower
						char [] s = rows[ypos+1].toCharArray();
						s [xpos] = '.';
						rows[ypos+1] = new String(s);
					} else {
						System.out.println("Bug!");
						System.exit(0);
					}
					break;
				}
				if (down) {
					if (ypos == 0) {
						System.out.println("Bug!");
						System.exit(0);
					}
					if (rows[ypos-1].charAt(xpos) == 'I') {   // delete icetower
						char [] s = rows[ypos-1].toCharArray();
						s [xpos] = '.';
						rows[ypos-1] = new String(s);
					} else {
						System.out.println("Bug!");
						System.exit(0);
					}
					break;
				}
			}
		}
	}
	
	private static void validState(int xpos, int ypos, String [] rows) {
		if (xpos > 7 || xpos < 0 || ypos > 7 || ypos < 0) {
			System.out.println("Bug!");
			System.exit(0);
		}
		if (rows[ypos].charAt(xpos) == 'I' || rows[ypos].charAt(xpos) == 'C' ) {
			System.out.println("Bug!");
			System.exit(0);
		 }
		if (rows[ypos].charAt(xpos) == 'D') {
			System.out.println("Diamond!");
			System.exit(0);
		 }
	}

}
