package caloriesFromFat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Calories {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader (new InputStreamReader (System.in));
        double [] Calories = {9,4,4,4,7};
        int jumpOut = 0;
        
        for (int cases = 0; cases < 150; cases++) {
            double calories = 0;
            double [] CaloriesEaten = {0,0,0,0,0};
            for (int lines = 0; lines < 100; lines++) {
                double caloriesThisMeal = 0;
                double percentage = 100;
        StringTokenizer st = new StringTokenizer(reader.readLine());
        String ss = st.nextToken();
        if (ss.charAt(0) == '-') {
            jumpOut++;
            break;}
        if (jumpOut != 0 && jumpOut == 1)
            jumpOut--;
        
        String [] nutrients = {ss, st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken()};
    
        
        // first iteration to convert from grams to calories 
        for (int i = 0; i < nutrients.length; i++) {
            if (nutrients[i].charAt(nutrients[i].length()-1) == 'g') {
                nutrients[i] = nutrients[i].substring(0,nutrients[i].length()-1);
                calories += (Double.parseDouble(nutrients[i])*Calories[i]);
                caloriesThisMeal += Double.parseDouble(nutrients[i])*Calories[i];
                CaloriesEaten[i] += Calories[i] * Double.parseDouble(nutrients[i]);}
            
            if (nutrients[i].charAt(nutrients[i].length()-1) == 'C') {
                nutrients[i] = nutrients[i].substring(0,nutrients[i].length()-1);
                calories += Double.parseDouble(nutrients[i]);
                caloriesThisMeal += Double.parseDouble(nutrients[i]);
                CaloriesEaten[i] += Double.parseDouble(nutrients[i]);}
            
            if (nutrients[i].charAt(nutrients[i].length()-1) == '%')
                percentage = percentage - Double.parseDouble(nutrients[i].substring(0,nutrients[i].length()-1));
        }
        double N = caloriesThisMeal/percentage;
        double d = 0;
        // Seccond iteration to convert from percentages to calories.
        for (int i = 0; i < nutrients.length; i++) {
            if (nutrients[i].charAt(nutrients[i].length()-1) == '%') {
                d = Double.parseDouble(nutrients[i].substring(0, nutrients[i].length()-1))*N;
                CaloriesEaten[i] += d;
                calories += d;}
        }
            }
        if (jumpOut != 2) {
        System.out.println(Math.round(CaloriesEaten[0]/(calories/100)) + "%");
        }
        if (jumpOut == 2)
            System.exit(0);;
        
             
        }
         
    
        

    }

}