package sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;



public class Sort {
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer ts = new StringTokenizer(reader.readLine());
		int n = Integer.parseInt(ts.nextToken());


		Queue<Integer> inp = new LinkedList<Integer>();
		
		Map<Integer, Integer> numbers = new HashMap<Integer,Integer>();

		
		ts = new StringTokenizer(reader.readLine());
		for (int i = 0; i < n; i++) {
			int j = Integer.parseInt(ts.nextToken());
			if (numbers.containsKey(j)) {
				int val = numbers.get(j);
				val++;
				numbers.put(j, val);
				
			}else {
				inp.add(j);
				numbers.put(j, 1);
			}
		}
		int [] a = new int[numbers.size()];
		int [] b = new int[numbers.size()];
		
		for (int i = 0; i < a.length; i++ ) {
			a[i] = inp.peek();
			b[i] = numbers.get(inp.poll());
		}


		DoubleMergeSort mSort = new DoubleMergeSort();
		mSort.sort(b,a,0,b.length-1);
		
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[i]; j++) {
				System.out.print(a[i] + " ");
			}
		}	
	}
 }
