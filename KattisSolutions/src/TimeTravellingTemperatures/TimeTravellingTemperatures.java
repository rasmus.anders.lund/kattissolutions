package TimeTravellingTemperatures;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class TimeTravellingTemperatures {

	public static void main(String[] args) throws IOException {
		//FileReader fr=new FileReader("C:\\Users\\rasmu\\Downloads\\2.in");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(reader.readLine());
        double A = 0;
        double B = Double.parseDouble(st.nextToken());
        double decrement = Double.parseDouble(st.nextToken());
        double adec = 1;
        int count = 0;
        if (B == 0 && decrement == 1) {
        	System.out.println("ALL GOOD");
        	System.exit(0);
        }
        if (decrement == 1.0) {
        	System.out.println("IMPOSSIBLE");
        	System.exit(0);}
 
        // yn + x = n  >= n(y-1) = -x >= n = x/(1-y)
        System.out.printf("%.8f \n", B /(1.0-decrement));
	}

}
