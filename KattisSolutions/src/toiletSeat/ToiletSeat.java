package toiletSeat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ToiletSeat {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	   
		String s = reader.readLine();
		
		int rule1 = 0;
		int rule2 = 0;
		int rule3 = 0;
		
		for (int i = 1; i < s.length(); i++) {
			
			if (s.charAt(i) == 'U') {
				if (i == 1 && s.charAt(i-1) == 'U') {
					rule2++;
				}
				else {
					rule2 += 2;
				}
			}
			if (s.charAt(i) == 'D') {
				if(i==1 && s.charAt(i-1) == 'D') {
					rule1++;
				} else {
					rule1 +=2;
				}
			}
			if (s.charAt(i) == 'U') {
				if(i==1 && s.charAt(i-1) == 'D')
					rule1++;
			}
			if (s.charAt(i) == 'D') {
				if(i==1 && s.charAt(i-1) == 'U')
					rule2++;
			}
			
			
			if (s.charAt(i) != s.charAt(i-1)) {
				rule3++;
			}	
		}
		System.out.println(rule1);
		System.out.println(rule2);
		System.out.println(rule3);
	}

}
