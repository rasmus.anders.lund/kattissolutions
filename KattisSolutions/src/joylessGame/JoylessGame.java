package joylessGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class JoylessGame {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
	
		
		for (int i = 0; i < T; i++) {
			String word = reader.readLine();
			LinkedList<Character> lis = new LinkedList<Character>();
			for(int j = 0; j < word.length(); j++) {
				lis.add(word.charAt(j));
			}

			int u;
			if (word.charAt(0) != word.charAt(word.length()-1))
				u = word.length();
			else
				u = word.length()-1;
			if (u%2 == 0)
				System.out.println("Bash");	
			else
				System.out.println("Chikapu"); 		
		}
	}
}
