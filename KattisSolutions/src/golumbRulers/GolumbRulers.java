package golumbRulers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.TreeSet;


public class GolumbRulers {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String inn = "";
		
		while (reader.ready()) {
		
		inn = reader.readLine();
		
		StringTokenizer st = new StringTokenizer(inn);
		HashSet<Integer> set = new HashSet<Integer>();
		int i = st.countTokens();
		Object[] nums = new Integer[i];
		TreeSet<Integer> tree = new TreeSet<Integer>();
		int j = 0;
		int count = 0;
		boolean maybe = true;
		int smallest = 1;
		
		while (st.hasMoreTokens()){
			int t = Integer.parseInt(st.nextToken());
			tree.add(t);
		}
		nums = tree.toArray();
		//int [] all = new int[nums[i-1]];
		
		
		j = (int)nums[i-1];
	
		outerloop:
		while(!tree.isEmpty()) {
			int a = tree.pollLast();
			if (a != 0) {
			for (int nu : tree) {
				int b = nu;
			int result = a-b;
			
			count++;
			if (set.contains(result)) {
				System.out.println("not a ruler");
				maybe = false;
				break outerloop;
				}
			set.add(result);
			 }
			}
		}
		
		int c = 0;
		if (count < j  && maybe) {
		int miss = j-count;
		System.out.print("missing ");
		
		for (int x = 1; x <= j; x++) {
			if (!set.contains(x)) {
				System.out.print(x + " ");
				c++;
				}
			if (c == miss) {
				System.out.println();
				break;
			}
		}
		} else if (maybe){
			System.out.println("perfect");
		}
	 }
	}

}
