package traffic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Traffic {

	public static void main(String[] args) throws IOException {
		 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		 StringTokenizer st1 = new StringTokenizer(reader.readLine());
		 int xPos1 = Integer.parseInt(st1.nextToken());
		 int xPos2 = Integer.parseInt(st1.nextToken());
		 
		 st1 = new StringTokenizer(reader.readLine());
		 StringTokenizer st2 = new StringTokenizer(reader.readLine());
		 
		 if (xPos1 < xPos2)
			 xPos1 += 4;
		 else
			 xPos2 +=4;
		
		 int car1moves = Integer.parseInt(st1.nextToken());
		 int car2moves = Integer.parseInt(st2.nextToken());
		 int car1Move = 0;
		 int car2Move = 0;
		 
		 if (car1moves >= 1) 
			 car1Move = Integer.parseInt(st1.nextToken());
		
		 if (car2moves >= 1) 
			  car2Move = Integer.parseInt(st2.nextToken());
		 
		 int time  = 1;
		 int lastMove = Math.max(car1Move, car2Move);
		 boolean car1Moving = false;
		 boolean car2Moving = false;
		 
		 while (true) {
			 if (xPos1 == xPos2) {
				 System.out.println("bumper tap at time " + (time-1));
				 break;
			 }
			
			 if (car1Moving)
				 xPos1++;
			 if (car2Moving)
				 xPos2++;
			 
			 if (time == car1Move) {
				 car1Moving = !car1Moving;
				 if(st1.hasMoreTokens())
					 car1Move = Integer.parseInt(st1.nextToken());
				 	if (car1Move >= lastMove)
				 		lastMove = car1Move;
			 }
			 if (time == car2Move) {
				 car2Moving = !car2Moving;
				 if(st2.hasMoreTokens())
					 car2Move = Integer.parseInt(st2.nextToken());
				 if (car2Move >= lastMove)
				 		lastMove = car2Move;
			 }
			 
			 if (xPos1 > xPos2  && time >= lastMove) {
				 if (!car2Moving) {
				 System.out.println("safe and sound");
				 break;
				 }
				 else if (car1Moving) {
					 System.out.println("safe and sound");
					 break; 
				 }
			 }
			 
			 if (xPos2 > xPos1  && time >= lastMove) {
				 if (!car1Moving) {
					 System.out.println("safe and sound ");
					 break;
				 }
					 else if (car2Moving) {
						 System.out.println("safe and sound ");
						 break;
					 }
			 }
			 
			 time++;
				 
			 
		 }

	}

}
